﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace addrwatchNotify
{
    
    // Enum Value must be the Index of the OptionSet
    public enum TypeEnum
    {
        ConfigFile = 0,
        Help = 1
    }

    public class Argument
    {
        public TypeEnum Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public static List<Argument> Arguments { get; set; } = new List<Argument>();
        
        public static void AddArgument(TypeEnum type, string name, string value)
        {
            Arguments.Add(new Argument { Type = type, Name = name, Value = value });
        }
        public static Argument GetArgument(TypeEnum type)
        {
            return Arguments.FirstOrDefault(x=>x.Type==type);
        }
        
        public static string GetConfigPath()
        {
            return GetArgument(TypeEnum.ConfigFile).Value;
        }
    }

}
