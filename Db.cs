﻿using System;
using System.Data;
using System.Diagnostics;

public interface IDb
{

    bool Connect();
    bool Disconnect();

    int BeginTransaction();
    void RollbackTransaction();
    void CommitTransaction();

    DataTable DoQuery(string sql);
    void PrintRecordset(ref DataTable dtData, string comment);
}

namespace addrwatchNotify
{
    public abstract class Db : IDb
    {
        protected string dataSource = "";
        protected int txNestLevel;                  // Holds the nesting level of transaction requests.

        public Db()
        {
        }

        public abstract bool Connect();
        public abstract bool Disconnect();

        public abstract int BeginTransaction();
        public abstract void RollbackTransaction();
        public abstract void CommitTransaction();

        public abstract DataTable DoQuery(string sql);


        public void PrintRecordset(ref DataTable dtData, string comment)
        {
            // Debug-Ausgabe
            Console.WriteLine(comment);
            foreach (DataRow row in dtData.Rows)
            {
                foreach (var item in row.ItemArray)
                {

                    if (item.ToString() == "")
                    {
                        Console.Write("- ");
                    }
                    else
                    {
                        Console.Write(item + " ");
                    }
                }
                Console.WriteLine("");
            }
            Console.WriteLine("");
        }
    }
}