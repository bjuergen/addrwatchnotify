﻿using System;
using System.Data;
using System.Data.SQLite;

namespace addrwatchNotify
{
    public class DbSQLite : Db
    {

        private SQLiteConnection connection;
        private SQLiteTransaction transaction;  // Stores a reference to the database transaction.

        public DbSQLite(string inDataSource)
        {
            this.dataSource = inDataSource;
        }


        public SQLiteConnection Connection
        {
            get
            {
                if (connection == null)
                    throw new InvalidOperationException("No valid connection.");

                return this.connection;
            }
        }

        public SQLiteCommand CreateCommand()
        {
            SQLiteCommand cmd = Connection.CreateCommand();
            cmd.Transaction = transaction;
            return cmd;
        }

        // Establish connection.
        public override bool Connect()
        {
            bool result = false;

            try
            {
                this.connection = new SQLiteConnection(dataSource);
                this.connection.Open();
                result = true;
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\n");
                this.connection = null;
            }

            return result;
        }


        // Cancel connection.
        public override bool Disconnect()
        {
            bool result = false;

            try
            {
                this.connection.Close();
                this.connection = null;
                result = true;
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\n");
            }

            return result;
        }

        public override int BeginTransaction()
        {
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection not open.");

            ++txNestLevel;

            if (transaction == null)
                transaction = connection.BeginTransaction();

            return txNestLevel;

        }

        public override void RollbackTransaction()
        {
            if (transaction == null)
                throw new InvalidOperationException("No active transaction.");

            if (--txNestLevel == 0)
            {
                transaction.Rollback();
                transaction = null;
            }
        }

        public override void CommitTransaction()
        {
            if (transaction == null)
                throw new InvalidOperationException("No active transaction.");

            if (--txNestLevel == 0)
            {
                transaction.Commit();
                transaction = null;
            }
        }

        public override DataTable DoQuery(string sql)
        {
            var cmd = new SQLiteCommand(connection);
            cmd.CommandText = sql;

            return DoQuery(cmd);
        }

        public DataTable DoQuery(SQLiteCommand cmd)
        {
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }

        /*
         Command API
         */
        public DataTable TestQuery()
        {
            return DoQuery("SELECT * FROM addrwatch");
        }

        public DataTable LastRecordQuery()
        {
            return DoQuery("SELECT  mac_address, timestamp  FROM addrwatch ORDER BY timestamp DESC LIMIT 1");
        }

        public DataTable NewRecordsQuery(string value)
        {
            return DoQuery("SELECT  mac_address, ip_address, timestamp  FROM addrwatch WHERE timestamp >= " + value);
        }

        public DataTable NotifyQuery()
        {
            return DoQuery("SELECT mac_address, ip_address, timestamp, notify FROM addrwatch_notify WHERE notify = 0");
        }

        public void CreateDataBase()
        {
            SQLiteConnection.CreateFile(dataSource);
        }

        public void CreateSQLiteTable()
        {
            BeginTransaction();
            try
            {
                SQLiteCommand cmd = CreateCommand();
                string sql;

                /* Lookup table */
                sql = "CREATE TABLE IF NOT EXISTS addrwatch_notify (`mac_address` VARCHAR(17), `ip_address` VARCHAR(42), `timestamp` UNSIGNEDBIGINT, `notify`  INTEGER DEFAULT 0, PRIMARY KEY(mac_address) )";

                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();


                CommitTransaction();
            }
            catch (SQLiteException ex)
            {
                RollbackTransaction();
                Console.WriteLine("Message: " + ex.Message + "\n");
            }

        }

        public void InsertMacQuery(ref DataTable dtData)
        {                 
            SQLiteCommand cmd = CreateCommand();
            string sql;
            string mac_address = "-";
            string ip_address = "-";
            string timestamp = "-";

      
                try
                {
                    foreach (DataRow row in dtData.Rows)
                    {
                        mac_address = row.ItemArray[0].ToString();
                        ip_address = row.ItemArray[1].ToString();
                        timestamp = row.ItemArray[2].ToString();

                        var dtData2 = new DataTable();
                        dtData2 = DoQuery("SELECT * FROM addrwatch_notify WHERE mac_address = '" + mac_address + "'");

                        // If record does not exist...
                        if (dtData2.Rows.Count == 0)
                        {
                            /* ...insert a new record. */
                            sql = "INSERT INTO addrwatch_notify(mac_address, ip_address, timestamp) VALUES('" + mac_address + "', '"+ ip_address +"', '" + timestamp + "')";
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                            Console.WriteLine("Insert new value: "+ mac_address);
                        }
                        else
                        {
                            Console.WriteLine("Value already exists: " + mac_address);
                        }
                    }

                }
                catch (SQLiteException ex)
                {
                    Console.WriteLine("Message: " + ex.Message + " " + mac_address + "\n");
                }

        }

        public void UpdateNotifyQuery(ref DataTable dtData)
        {
            string sql;
            string mac_address = "-";
            string ip_address = "-";

            int count = 0;
            SQLiteCommand cmd = CreateCommand();

            try
            {
                foreach (DataRow row in dtData.Rows)
                {
                   mac_address = row.ItemArray[0].ToString();
                   ip_address = row.ItemArray[1].ToString();
                   long unixtime = System.Convert.ToInt64(row.ItemArray[2]);
                   string tdfirst = Tools.UnixTimeToDateTime(unixtime).ToString(); // DateTime
                   
                   /* Update 'notify' field. */
                   sql = "UPDATE addrwatch_notify SET notify=1 WHERE mac_address = '" + mac_address +"'";
                   cmd.CommandText = sql;
                   cmd.ExecuteNonQuery();

                    Tools.Email("MAC: " + mac_address + "  IP: "+ ip_address + "  Seen at: " + tdfirst);
                   count++;
                }
                Console.WriteLine(count + " value(s) updated.");

            }
            catch (SQLiteException ex)
            {
                Console.WriteLine("Message: " + ex.Message);
            }

        }
    }
}