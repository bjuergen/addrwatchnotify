﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace addrwatchNotify
{
    public static class Extensions
    {
        public static Int32 ToInt32(this string s)
        {
            Int32 ret;
            bool b;
            b = Int32.TryParse(s,out ret);
            if (!b)
                throw new Exception($"Cannot convert '{s}' to int32");
            return ret;
        }
    }
}
