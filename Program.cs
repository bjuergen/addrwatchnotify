﻿using System;
using System.Data;
using System.Threading;
using System.Collections.Generic;
using NDesk.Options;
using System.Reflection;

namespace addrwatchNotify
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = "Settings.xml";
            bool help = false;

            // dont forget '-' and '--' ,
            OptionSet p = new OptionSet() {
                { "c|configfile=", v => data = v },
                { "h|help", v => help = v != null },
            };
            // We don't parse extra arguments at the moment.
            //List<string> extra = p.Parse(args);

            //Console.WriteLine ("SettingsFile: {0}",data);
            
            // TODO: We should store arguments in an extra
            // data structure.
            //Argument.AddArgument(TypeEnum.ConfigFile, "", data);


            if (help == true)
            {
                Console.WriteLine("addrwatchNotify version 0.22");
                Console.WriteLine("configfile={0}\n", data);
                Console.WriteLine("Available options:");
                Console.WriteLine("-c   --configfile=<filename>");
                Console.WriteLine("-h   --help this message");
            }

            // Load Settings-File
            Settings.SettingsManager.LoadSettings(Assembly.GetExecutingAssembly(), data);

            // Extract ConnectionString from Settings-File
            string connectionString = Settings.SettingsManager.Reader.GetConnectionString();

            // Extract LoopInterval from Settings-File
            int interval = Settings.SettingsManager.Reader.GetLoopInterval();
            if (interval < 1000) { interval = 1000; }

            Console.WriteLine(connectionString);
            Console.WriteLine($"LoopInterval: {interval}");

             /* Connect to database. */
            var db = new DbSQLite(connectionString);

            bool state = db.Connect();
            if (state == true)
            {
                
                Console.WriteLine("Successfully connected to DB!\n");
                db.CreateSQLiteTable();

                while (true) {

                // Find last record in table.
                DataTable dt = db.LastRecordQuery();
                DataRow row = dt.Rows[0];

                string maclast = row.ItemArray[0].ToString(); // MAC
                string tslast = row.ItemArray[1].ToString(); // Unix timestamp
                long unixtime = System.Convert.ToInt64(row.ItemArray[1]);
                string tdlast = Tools.UnixTimeToDateTime(unixtime).ToString(); // DateTime

                Console.WriteLine("--- BEGIN LOOP ---");
                Console.WriteLine("LastRecord:\n" + maclast + " " + tdlast + " " + tslast + "\n");
                 

                Thread.Sleep(interval);

                // Find new records in table.
                DataTable dt2 = db.NewRecordsQuery(tslast);
                db.PrintRecordset(ref dt2, "LastRecord + NewRecords: ");
                Console.WriteLine("Insert:");
                db.InsertMacQuery(ref dt2);

                // Notify for new devices, not 
                // in table 'addrwatch_notify'.
                DataTable dt3 = db.NotifyQuery();
                db.PrintRecordset(ref dt3, "\nNotify:");
                db.UpdateNotifyQuery(ref dt3);

                Console.WriteLine("--- END LOOP ---\n\n");
                }
               
            }
            else
            {
                Console.WriteLine("Couldn't connected to DB!");
                Console.ReadKey();
            }
           
        }
    }
}
