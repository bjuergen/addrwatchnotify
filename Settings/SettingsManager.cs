﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace addrwatchNotify.Settings
{
    public static class SettingsManager
    {
        public static string _settingsFilePath="";
        public static SettingsReader Reader { get; set; } = new SettingsReader();
        public static void LoadSettings(Assembly a, string settingsFileName)
        {
            // Locate Filepath
            FileInfo fi = new FileInfo(a.Location);
            _settingsFilePath = $"{fi.DirectoryName}/{settingsFileName}";

            Reader.Open(_settingsFilePath);
        }
    }
}
