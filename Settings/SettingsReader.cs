﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace addrwatchNotify.Settings
{
    public class SettingsReader
    {
        public class Setting
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public string Type { get; set; }

            public long LoopInterval { get; set; }
            public string SMTPHost { get; set; }
            public string EmailAddressSender { get; set; }
            public string EmailAddressRecipient { get; set; }
            public string Subject { get; set; }

            public static Setting Parse(DataRow row)
            {
                Setting s = new Setting();
                s.Name = row["Name"].ToString();
                s.Value = row["Value"].ToString();
                s.Type = row["Type"].ToString();
                return s;
            }
        }

        private Setting GetRow(string name)
        {
            string filter = $"Name='{name}'";
            DataRow row = _dtSettings.Select(filter)[0];
            return Setting.Parse(row);
        }

        private Setting GetConnectionStringSetting()
        {
            Setting s = GetRow("ConnectionString");
            return s;
        }
        public string GetConnectionString()
        {
            return GetConnectionStringSetting().Value;
        }

        private Setting GetLoopIntervalSetting()
        {
            Setting s = GetRow("LoopInterval");
            return s;
        }
        public Int32 GetLoopInterval()
        {
            return GetLoopIntervalSetting().Value.ToInt32();
        }


        private Setting GetSMTPHostSetting()
        {
            Setting s = GetRow("SMTPHost");
            return s;
        }
        public string GetSMTPHost()
        {
            return GetSMTPHostSetting().Value;
        }

        private Setting GetEmailAddressSenderSetting()
        {
            Setting s = GetRow("EmailAddressSender");
            return s;
        }
        public string GetEmailAddressSender()
        {
            return GetEmailAddressSenderSetting().Value;
        }
        
        private Setting GetEmailAddressRecipientSetting()
        {
            Setting s = GetRow("EmailAddressRecipient");
            return s;
        }
        public string GetEmailAddressRecipient()
        {
            return GetEmailAddressRecipientSetting().Value;
        }


        private Setting GetSubjectSetting()
        {
            Setting s = GetRow("Subject");
            return s;
        }
        public string GetSubject()
        {
            return GetSubjectSetting().Value;
        }





        DataTable _dtSettings;
        public void Open(string filePath)
        {
            DataSet dsSettings;
            dsSettings = new DataSet();
            dsSettings.ReadXml(filePath);
            _dtSettings = dsSettings.Tables["Setting"];
        }

        public void Close()
        {

        }
    }
}
