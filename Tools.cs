﻿using System;
using System.Net;
using System.Net.Mail;

namespace addrwatchNotify
{
    public class Tools
    {
        // Load Settings
        static string s1 = Settings.SettingsManager.Reader.GetEmailAddressSender();
        static string s2 = Settings.SettingsManager.Reader.GetEmailAddressRecipient();
        static string s3 = Settings.SettingsManager.Reader.GetSubject();
        static string s4 = Settings.SettingsManager.Reader.GetSMTPHost();

        public Tools()
        {
        }

        /// <summary>
        /// Convert Unix time value to a DateTime object.
        /// </summary>
        /// <param name="unixtime">The Unix time stamp you want to convert to DateTime.</param>
        /// <returns>Returns a DateTime object that represents value of the Unix time.</returns>
        public static DateTime UnixTimeToDateTime(long unixtime)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixtime).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        /// Convert a DateTime to a unix timestamp
        /// </summary>
        /// <param name="MyDateTime">The DateTime object to convert into a Unix Time</param>
        /// <returns>Returns a Unix timestamp.</returns>
        public static long DateTimeToUnix(DateTime MyDateTime)
        {
            TimeSpan timeSpan = MyDateTime - new DateTime(1970, 1, 1, 0, 0, 0);

            return (long)timeSpan.TotalSeconds;
        }

        /// <summary>
        /// Generate a timestamp for logfiles
        /// </summary>
        /// <returns>Returns a timestamp string</returns>
        public static string Timestamp()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");;
        }

        /// <summary>
        /// Send a notification email
        /// </summary>
        /// <param name="msg">The message you want to send</param>
        public static void Email(string msg) {  
        try {  
            MailMessage message = new MailMessage();  
            SmtpClient smtp = new SmtpClient();  

            message.From = new MailAddress(s1);  
            message.To.Add(new MailAddress(s2));  
            message.Subject = s3;  
            message.IsBodyHtml = false; //to make message body as plain text
            message.Body = msg;  
            smtp.Port = 25;  
            smtp.Host = s4; // SMTP-Host
            smtp.EnableSsl = false;  
            smtp.UseDefaultCredentials = false;  
                smtp.Credentials = new NetworkCredential(s1, "password");  
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;  
            smtp.Send(message);  
            Console.WriteLine("Email sent.");
            } 
            catch (Exception ex) {
                Console.WriteLine("Message: " + ex.Message);
            }  
        }  
    }
}